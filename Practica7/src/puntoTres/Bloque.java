package puntoTres;

import java.awt.image.BufferedImage;


public class Bloque {
	private BufferedImage img1;
	private BufferedImage img2;
	private BufferedImage imgR;
	private String simbolo;
	private int alfa;
	private int beta;
	private boolean asignado = false;
	
	public Bloque(String sim, BufferedImage A, BufferedImage B)
	{
		this.simbolo = sim;
		this.img1 = A;
		this.img2 = B;
	}
	
	public BufferedImage getResult(){	return imgR;	} 
	
	public void setAlfa(int nA){	alfa = nA;	}
	
	public void setBeta(int nB){	beta = nB;	}
	
	public void ejecuta()
	{
		switch(simbolo){
		case " + ":
			imgR = ImageOperator.suma(img1, img2);
			break;
		case " - ":
			imgR = ImageOperator.resta(img1, img2);
			break;
		case " * ":
			imgR = ImageOperator.multiplicacion(img1, img2);
			break;
		case "\u03B1 - \u03B2":
			imgR = ImageOperator.combinacionLineal(img1, img2, alfa, beta);
			break;
		}
		this.asignado = true;
	}
	
	public void setAsignado(boolean n){	asignado = n;}
	
	public boolean getAsignado(){	return asignado;}
	
}

