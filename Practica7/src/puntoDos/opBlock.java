package puntoDos;

import java.awt.image.BufferedImage;

public class opBlock {

	public static BufferedImage blockOperate(String simb,BufferedImage imgA, BufferedImage imgB, 
			int nFilas, int nCol,int alfa, int beta)
	{
		int tamW = imgA.getWidth() / nFilas;
		int inicioW  = 0;
		int finW = tamW;
		int tamH = imgA.getHeight() / nCol;
		int inicioH = 0;
		int finH = tamH;
		int nBloques = nFilas * nCol;
		
		BufferedImage[] res = new BufferedImage[nBloques];
		Operar[] partes = new Operar[nBloques];
		for(int i = 0; i < nBloques; i++)
		{			
			Operar parte = new Operar(simb, inicioW, finW, inicioH, finH, imgA, imgB);
			if(simb == "\u03B1 - \u03B2"){
				parte.setAlfa(alfa);
				parte.setBeta(beta);
			}
			partes[i] = parte;
			if((i+1) % nFilas != 0){
				inicioW = inicioW + tamW;
				finW = finW + tamW;
			}else{
				inicioW = 0;
				finW = tamW;
				inicioH = inicioH + tamH;
				finH = finH + tamH;
			}
		}
		
		try {
			for (int i = 0; i < nBloques; i++)
			{
				partes[i].ejecuta();
				res[i] = partes[i].getResult();
			}
			return (ImageOperator.acopla(res, nFilas, nCol));
		} catch (Exception e) {};

	return null;
	}
	
}
