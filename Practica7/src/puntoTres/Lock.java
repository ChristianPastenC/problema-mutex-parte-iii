package puntoTres;

public interface Lock {
	public void requestCR(int id);
	public void releaseCR(int id);
}
