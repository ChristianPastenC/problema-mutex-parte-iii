package puntoTres;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class opBlock {
	
	int nFilas, nColumnas, nBloques;
	BufferedImage[] blockA, blockB;
	Bloque[] blocks;
	
	public opBlock(BufferedImage A, BufferedImage B, int filas,int col){
		
		this.nFilas = filas;
		this.nColumnas = col;
		this.nBloques = nFilas * nColumnas;
		this.blockA = new BufferedImage[nBloques];
		this.blockB = new BufferedImage[nBloques];
		
		int tamW = A.getWidth() / nFilas;
		int inicioW  = 0;
		int finW = tamW;
		int tamH = A.getHeight() / nColumnas;
		int inicioH = 0;
		int finH = tamH;
		for(int i = 0; i < nBloques; i++)
		{
			blockA[i] = A.getSubimage(inicioW, inicioH, tamW, tamH);
			blockB[i] = B.getSubimage(inicioW, inicioH, tamW, tamH);
			if((i+1) % nFilas != 0){
				inicioW = inicioW + tamW;
				finW = finW + tamW;
			}else{
				inicioW = 0;
				finW = tamW;
				inicioH = inicioH + tamH;
				finH = finH + tamH;
			}
		}
	}
	
	public int contador()
	{
		int n = 0;
		for(int i = 0; i < blocks.length; i++)
		{
			if(blocks[i].getAsignado() == false)
				n++;
		}
	return n;
	}
	
	public void creaBloques(String simb, int alfa, int beta){
		this.blocks = new Bloque[nBloques];
		for(int i = 0; i < nBloques; i++)
		{
			Bloque parte = new Bloque(simb, blockA[i],blockB[i]);
			if(simb == "\u03B1 - \u03B2"){
				parte.setAlfa(alfa);
				parte.setBeta(beta);
			}
			this.blocks[i] = parte;
		}
	}
	
	public Bloque[] getBloques(){	return this.blocks;	}
	
	public int getNumBloques(){	return this.nBloques;	}
	
	public BufferedImage blockOperate()
	{
		ArrayList<BufferedImage> res = new ArrayList<BufferedImage>();
		
		try {
			for(int i = 0; i < nBloques; i++)
			{
				this.blocks[i].ejecuta();
				res.add(this.blocks[i].getResult());
			}
			return (ImageOperator.acopla(res, this.nFilas, this.nColumnas));
		} catch (Exception e) {};	
	return null;
	}
}
