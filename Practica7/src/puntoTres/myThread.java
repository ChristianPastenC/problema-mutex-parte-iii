package puntoTres;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import puntoTres.Lock;
import puntoTres.Util;

public class myThread extends Thread {
	
	public static ArrayList<BufferedImage> listImage = new ArrayList<BufferedImage>();
	
	int myId;
	Lock lock;
	Random r = new Random();
	
	//****
		Bloque[] block;
		boolean status = false;
		Bloque asignado;
		boolean terminado = false;
		BufferedImage imgR;
	//***
	
	public myThread( int id, Lock lock, Bloque[] pieza)
	{
		r = new Random();
		myId = id;
		this.lock = lock;
		block = pieza;
	}
	
	public void CriticalRegion(){
		if(status == false){
			for(int i = 0; i < block.length; i++)
			{
				if(block[i].getAsignado() == false){
					block[i].setAsignado(true);
					System.out.println(i);
					asignado = block[i];
					status = true;
					break;
				}
				if(i == (block.length - 1))
					terminado = true;
			}
		}else{
			status = false;
		}
		Util.mySleep( r.nextInt(1000));
	}
	
	public void nonCriticalRegion(){
		asignado.ejecuta();
		imgR = asignado.getResult();
		listImage.add(imgR);
		Util.mySleep( r.nextInt( 1000 ) );
	}
	
	public void run(){
		System.out.println("Running");
		while(terminado == false ){
			lock.requestCR( myId );
			CriticalRegion();
			lock.releaseCR( myId );
			nonCriticalRegion();
		}
	}
	
	public BufferedImage getImage(){
		return imgR;
	}
	
	public static BufferedImage parallelOperate (String simb,BufferedImage imgA, BufferedImage imgB, 
	int nFilas, int nCol,int alfa, int beta, int nThreads)
	{
		ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
		images = listImage;
		listImage.clear();
		
		opBlock aux = new opBlock(imgA, imgB, nFilas, nCol);
		aux.creaBloques(simb, alfa, beta);
		
		Bloque[] auxBloque = aux.getBloques();
		
		myThread hilos[] = new myThread[nThreads];
		Lock lock = new Bakery(nThreads);
		for(int i = 0; i < nThreads; i++)
		{
			myThread hilo = new myThread(i, lock, auxBloque);
			hilos[i] = hilo;
		}
		
		try{
			for(int i = 0; i < nThreads; i++)
			{
				hilos[i].start();
			}
			for(int i = 0; i < nThreads; i++)
			{
				try{
					hilos[i].join();
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
			return ImageOperator.acopla(images, nFilas, nCol);
		}catch(Exception e){e.printStackTrace();}
	return null;
	}
}
